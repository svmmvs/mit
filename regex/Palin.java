package regex;

/**
 * If both "i", and "n- i- 1" are ?, replace them by ‘a’
 * If one of them is ? character, replace that by other letter character
 * <p>
 * O(n) time, O(1) space
 */
public class Palin {

    public static void smallestPalin(String text) {
        char[] chars = text.toCharArray();
        int len = chars.length;

        for (int i = 0; i < len / 2; i++) {
            if (chars[i] != '?' && chars[len - i - 1] != '?' && chars[i] != chars[len - i - 1]) {
                System.out.println("NO PALIN");
                return;
            } else {
                // chars can be (? and letter), or (? and ?)
                if (chars[i] == '?') {
                    if (chars[len - i - 1] == '?') {
                        chars[i] = 'a';
                        chars[len - i - 1] = 'a';
                    } else {
                        chars[i] = chars[len - i - 1];
                    }
                    // char is letter, so if other side is ?, replace it
                } else if (chars[len - i - 1] == '?') {
                    chars[len - i - 1] = chars[i];
                }
            }
        }

        System.out.println(String.valueOf(chars));
    }

    public static void main(String[] args) {
        String text = "ab??e?c?a";
        smallestPalin(text);
    }
}
