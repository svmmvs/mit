package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPMatcher {
    static boolean validateIP(String test) {
        String IPV4 = "^(([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])([.](?!$)|$)){4}$";
        Pattern pattern = Pattern.compile(IPV4);
        Matcher matcher = pattern.matcher(test);
        return matcher.matches(); //.find() next occurrence substring
    }

    public static void main(String[] args) {
        System.out.println(validateIP("192.168.2.1"));
    }
}
