package graphs;

import java.util.ArrayList;
import java.util.List;

public class Graph {
    // adjacency list to represent vertices and edges
    List<List<Integer>> adj = new ArrayList<>();
    int numberVertices;

    Graph(List<Edge> edges, int numberVertices) {
        this.numberVertices = numberVertices;
        for (int i = 0; i < numberVertices; i++) {
            adj.add(new ArrayList<>());
        }

        for (Edge edge : edges) {
            int from = edge.from;
            int to = edge.to;
            adj.get(from).add(to);
            //adj.get(to).add(from); // if undirected graph
        }
    }
}
