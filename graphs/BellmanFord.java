package graphs;

import java.util.ArrayList;
import java.util.List;

/*
 * O(Vertices*Edges)
 */
public class BellmanFord {

    int[] dist;

    public static void main(String[] arg) {
        int V = 4;
        int source = 1;
        List<List<Node>> adj = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            List<Node> neighbours = new ArrayList<>();
            adj.add(neighbours);
        }
        // important to start numbering from 0 because of dist index
        adj.get(3).add(new Node(1, 2));
        adj.get(1).add(new Node(2, 3));
        adj.get(2).add(new Node(0, 2));

        BellmanFord bf = new BellmanFord();
        bf.bellmanFord(adj, source, V);
        System.out.println("The shorted path from node :");
        for (int i = 0; i < bf.dist.length; i++) {
            System.out.println(source + " to " + i + " is " + bf.dist[i]);
        }
    }

    public void bellmanFord(List<List<Node>> adj, int source, int V) {
        dist = new int[V];
        for (int i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
        }
        // distance to the source is 0
        dist[source] = 0;
        int edgeDistance;
        int newDistance;
        // https://riptutorial.com/algorithm/example/24029/why-do-we-need-to-relax-all-the-edges-at-most--v-1--times
        // all edges need to be relaxed V-1 times, since start is arbitrary (not necessarily at k = 0)
        for (int t = 1; t < V; t++) {
            // relax all edges (= vertices + adjacency lists)
            for (int k = 0; k < adj.size(); k++) {
                for (int i = 0; i < adj.get(k).size(); i++) {
                    Node v = adj.get(k).get(i);
                    edgeDistance = v.weight;
                    newDistance = dist[k] + edgeDistance;
                    // newDistance overflows if dist[k] is MAX_VALUE
                    if (dist[k] != Integer.MAX_VALUE && dist[v.node] > newDistance) {
                        dist[v.node] = newDistance;
                    }
                }
            }
        }

        // check all edges one more time
        for (int k = 0; k < adj.size(); k++) {
            for (int i = 0; i < adj.get(k).size(); i++) {
                Node v = adj.get(k).get(i);
                edgeDistance = v.weight;
                newDistance = dist[k] + edgeDistance;
                if (dist[k] != Integer.MAX_VALUE && dist[v.node] > newDistance) {
                    System.out.println("negative weight cycle");
                    return;
                }
            }
        }
    }
}
