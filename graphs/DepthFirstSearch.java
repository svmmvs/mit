package graphs;

import java.util.*;

public class DepthFirstSearch {

    public static void depthFirstSearchRecursive(Graph graph, int source, boolean[] seen, List<Integer> res) {
        seen[source] = true;
        res.add(source);
        for (int u : graph.adj.get(source)) {
            if (!seen[u]) {
                depthFirstSearchRecursive(graph, u, seen, res);
            }
        }
    }

    public static List<Integer> depthFirstSearchIterative(Graph graph, int source) {
        List<Integer> res = new ArrayList<>(graph.numberVertices);
        HashMap<Integer, Integer> parents = new HashMap<>();
        Deque<Integer> stack = new ArrayDeque<>();
        parents.put(source, null); // source's parent is null
        stack.addFirst(source);
        while (!stack.isEmpty()) {
            source = stack.removeFirst();
            res.add(source); // check searched vertex
            List<Integer> current = graph.adj.get(source);
            ListIterator<Integer> iterator = current.listIterator(current.size());
            while (iterator.hasPrevious()) { // iterate in reverse order for expected recursive order
                int u = iterator.previous();
                if (!parents.containsKey(u)) {
                    parents.put(u, source);
                    stack.addFirst(u);
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        List<Edge> edges = Arrays.asList(new Edge(1, 2), new Edge(1, 3), new Edge(1, 4), new Edge(2, 5), new Edge(2, 6), new Edge(5, 7), new Edge(5, 8), new Edge(8, 9));
        // vertex 0 is single vertex
        int number = 10;
        Graph graph = new Graph(edges, number);

        List<Integer> res = new ArrayList<>(graph.numberVertices);
        boolean[] seen = new boolean[graph.numberVertices];
        depthFirstSearchRecursive(graph, 0, seen, res);
        System.out.println("depth first search from 0: " + res);

        List<Integer> res1 = new ArrayList<>(graph.numberVertices);
        boolean[] seen1 = new boolean[graph.numberVertices];
        depthFirstSearchRecursive(graph, 1, seen1, res1);
        System.out.println("depth first search from 1: " + res1);

        System.out.println("depth first search iterative from 0: " + depthFirstSearchIterative(graph, 0));
        System.out.println("depth first search iterative from 1: " + depthFirstSearchIterative(graph, 1));
    }
}
