package graphs;

import java.util.*;

/*
 * O(Edges*log(Vertices))
 */
public class Dijkstra {

    int[] dist;
    int[] parents;
    List<List<Node>> adjacency;
    PriorityQueue<Node> todo;
    Set<Integer> done = new HashSet<>();

    public static void main(String[] arg) {
        int V = 4;
        int source = 1;
        List<List<Node>> adj = new ArrayList<>();
        for (int i = 0; i < V; i++) {
            adj.add(new ArrayList<>());
        }
        // important to start numbering from 0 because of arrays
        adj.get(3).add(new Node(1, 2));
        adj.get(1).add(new Node(2, 3));
        adj.get(2).add(new Node(0, 2));

        Dijkstra dij = new Dijkstra();
        dij.dijkstra(adj, source, V);
        System.out.println("The shorted path from node :");
        for (int i = 0; i < dij.dist.length; i++) {
            System.out.println(source + " to " + i + " is " + dij.dist[i]);
        }
    }

    public void dijkstra(List<List<Node>> adj, int source, int V) {
        adjacency = adj;
        dist = new int[V];
        parents = new int[V];
        for (int i = 0; i < V; i++) {
            dist[i] = Integer.MAX_VALUE;
        }
        todo = new PriorityQueue<>(V, new Node());
        todo.add(new Node(source, 0)); // source with cost 0
        dist[source] = 0; // distance to source 0

        while (!todo.isEmpty()) {
            int u = todo.poll().node; // extract_min from Q
            if (done.contains(u)) continue; // u already in done list
            done.add(u);
            relaxate(u); // relax all vertices in adjacency list of u
        }
    }

    public void relaxate(int u) {
        int edgeDist;
        int newDist;
        for (int i = 0; i < adjacency.get(u).size(); i++) {
            Node v = adjacency.get(u).get(i);
            if (!done.contains(v.node)) {
                edgeDist = v.weight;
                newDist = dist[u] + edgeDist;
                if (dist[v.node] > newDist) {
                    dist[v.node] = newDist; // newDist is lighter
                    parents[v.node] = u;
                    todo.add(new Node(v.node, dist[v.node]));
                }
            }
        }
    }
}
