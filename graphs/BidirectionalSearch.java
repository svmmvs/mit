package graphs;

import java.util.*;

/*
 * transform word 1 to word 2 using a dictionary of given words
 * every transform (adjacent pair of words) differ by single letter
 * return number of words in the shortest transformation sequence from word 1 to word 2, 0 if not exists
 * word 1 length = word 2 length = dictionary words length
 * all lowercase english
 * word 1 != word 2
 * dictionary words all unique
 *
 * example: word 1: "hit", word 2: "dot", dictionary: ["hot", "lot", "dog", "dot"]
 * sequence of words: "hit" to "hot" to "dot", 3 words long, return 3
 */
public class BidirectionalSearch {

    public static void main(String[] args) {
        String start = "hit";
        String end = "dot";
        List<String> dict = new ArrayList<>();
        dict.add("hot");
        dict.add("lot");
        dict.add("dog");
        dict.add("dot");
        System.out.println(ladderLength(start, end, dict));
    }

    // n size of dictionary, m length of word -> O(n*m*m)
    public static int ladderLength(String beginWord, String endWord, List<String> wordList) {
        HashSet<String> beginSet = new HashSet<>(wordList);
        HashSet<String> endSet = new HashSet<>(wordList);
        Queue<String> beginQ = new ArrayDeque<>();
        Queue<String> endQ = new ArrayDeque<>();
        // 'endWord' must be in wordList
        if (!beginSet.contains(endWord)) {
            return 0;
        }
        // remove 'endWord' from endSet in case of hitting 'endWord' at the first search
        else {
            endSet.remove(endWord);
        }
        int res = 1;
        beginQ.add(beginWord);
        endQ.add(endWord);

        while (!beginQ.isEmpty() && !endQ.isEmpty()) {
            // optimization: decrease the number of nodes that we have to search
            if (beginQ.size() > endQ.size()) {
                // swap queues and sets for bidirectional
                Queue<String> temp = beginQ;
                beginQ = endQ;
                endQ = temp;
                HashSet<String> tempSet = beginSet;
                beginSet = endSet;
                endSet = tempSet;
            }

            int size = beginQ.size();
            res++;

            for (int i = 0; i < size; i++) {
                String current = beginQ.remove();
                // transformation process: replace from first to last char with a-z each and check if in dictionary
                for (int j = 0; j < current.length(); j++) {
                    StringBuilder builder = new StringBuilder(current);
                    for (char c = 'a'; c <= 'z'; c++) {
                        builder.setCharAt(j, c);
                        String transformed = builder.toString();
                        if (beginSet.contains(transformed)) {
                            if (!endSet.contains(transformed) || transformed.equals(endWord)) {
                                return res;
                            }
                            beginSet.remove(transformed);
                            beginQ.add(transformed);
                        }
                    }
                }
            }
        }

        return 0;
    }
}
