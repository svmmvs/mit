package graphs;

import java.util.ArrayList;
import java.util.List;

/*
 * O(Vertices + Edges)
 * auxiliary space: O(Vertices) for finishing times
 *
 * for all edges (no back edges in DAG): edge (u, v): departure[u] > departure[v]
 * largest departure time -> ... -> smallest departure time
 */
public class TopologicalSort {

    public static List<Integer> topologicalSort(List<List<Integer>> adj) {
        int N = adj.size();
        // departure stores the vertex number using departure time as an index
        int[] departure = new int[N];
        // If one fills array with departure time using vertex number as index, then need to sort it later

        // to keep track of whether a vertex is seen or not
        boolean[] seen = new boolean[N];
        int time = -1; // to start at index 0

        // perform DFS on all not seen vertices
        for (int k = 0; k < N; k++) {
            if (!seen[k]) {
                time = depthFirstSearch(adj, k, seen, departure, time);
            }
        }

        List<Integer> res = new ArrayList<>();
        // get vertices in reverse order of departure time in DFS = topological order
        for (int i = N - 1; i >= 0; i--) {
            res.add(departure[i]);
        }
        return res;
    }

    public static int depthFirstSearch(List<List<Integer>> adj, int v, boolean[] seen, int[] departure, int time) {
        seen[v] = true;
        for (int u : adj.get(v)) {
            if (!seen[u]) {
                time = depthFirstSearch(adj, u, seen, departure, time);
            }
        }
        // ready to backtrack, set departure time of vertex v, fill in smallest to the largest time
        time++;
        departure[time] = v;
        return time;
    }

    public static void main(String[] args) {
        // adjacency list to represent DAG
        final int N = 6;
        List<List<Integer>> adjList = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            adjList.add(new ArrayList<>());
        }
        adjList.get(5).addAll(List.of(0, 2));
        adjList.get(4).addAll(List.of(0, 1));
        adjList.get(2).add(3);
        adjList.get(3).add(1);
        List<Integer> sorted = topologicalSort(adjList);
        System.out.println("Topological Order: " + sorted);
    }
}
