package graphs;

import java.util.*;

public class BreadthFirstSearch {

    public static List<Integer> breadthFirstSearchIterative(Graph graph, int source) {
        List<Integer> res = new ArrayList<>(graph.numberVertices);
        boolean[] seen = new boolean[graph.numberVertices];
        Deque<Integer> queue = new ArrayDeque<>();
        seen[source] = true;
        queue.addLast(source);
        while (!queue.isEmpty()) {
            source = queue.removeFirst();
            res.add(source); // check searched vertex
            for (int u : graph.adj.get(source)) {
                if (!seen[u]) {
                    seen[u] = true;
                    queue.addLast(u);
                }
            }
        }
        return res;
    }

    public static List<Integer> breadthFirstSearchRecursive(Graph graph, Deque<Integer> queue, boolean[] seen, List<Integer> res) {
        if (queue.isEmpty()) {
            return res;
        }
        int source = queue.removeFirst();
        res.add(source); // check searched vertex
        for (int u : graph.adj.get(source)) {
            if (!seen[u]) {
                seen[u] = true;
                queue.addLast(u);
            }
        }
        return breadthFirstSearchRecursive(graph, queue, seen, res);
    }

    public static void main(String[] args) {
        List<Edge> edges = Arrays.asList(new Edge(1, 2), new Edge(1, 3), new Edge(1, 4), new Edge(2, 5), new Edge(2, 6), new Edge(5, 7), new Edge(5, 8), new Edge(8, 9));
        // vertex 0 is single vertex
        int number = 10;
        Graph graph = new Graph(edges, number);
        System.out.println("breadth first search from 0: " + breadthFirstSearchIterative(graph, 0));
        System.out.println("breadth first search from 1: " + breadthFirstSearchIterative(graph, 1));

        List<Integer> res = new ArrayList<>(graph.numberVertices);
        boolean[] seen = new boolean[graph.numberVertices];
        Deque<Integer> queue = new ArrayDeque<>();
        for (int i = 0; i < graph.numberVertices; i++) {
            if (!seen[i]) {
                seen[i] = true;
                queue.addLast(i);
                System.out.println("breadth first search recursive: " + breadthFirstSearchRecursive(graph, queue, seen, res));
            }
        }
    }
}
