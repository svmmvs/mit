package trees;

/*
 * O(n) number of nodes
 * auxiliary space: O(h) height of tree
 */
public class BinaryTreeMaxPathSum {

    private static int maximum = Integer.MIN_VALUE;

    public static int maxPathSum(TreeNode root) {
        getMax(root);
        return maximum;
    }

    public static int getMax(TreeNode node) {
        if (node == null) return 0;

        int left = Math.max(getMax(node.left), 0);
        int right = Math.max(getMax(node.right), 0);

        maximum = Math.max(maximum, node.val + left + right);
        int max = Math.max(left, right);
        return node.val + max;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        System.out.println("5 - 2 - 1 - 3 max path: " + maxPathSum(root));
    }
}
