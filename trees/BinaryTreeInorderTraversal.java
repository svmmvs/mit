package trees;

import java.util.*;

class BinaryTreeInorderTraversal {
    /*
     * Iterative with stack and temporary node
     */
    public static List<Integer> inorderTraversalIterative(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Deque<TreeNode> stacks = new ArrayDeque<>();
        TreeNode node = root;

        while (node != null || !stacks.isEmpty()) {
            if (node != null) {
                stacks.push(node);
                node = node.left;
            } else {
                node = stacks.pop();
                res.add(node.val);
                node = node.right;
            }
        }
        return res;
    }

    /*
     * Recursive with helper function (straightforward)
     */
    public static List<Integer> inorderTraversalRecursive(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        inorderRecursive(root, res);
        return res;
    }

    private static void inorderRecursive(TreeNode node, List<Integer> res) {
        if (node != null) {
            inorderRecursive(node.left, res);
            res.add(node.val);
            inorderRecursive(node.right, res);
        }
    }

    /*
     * Morris traversal with threaded binary tree
     */
    public static List<Integer> inorderMorrisTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        TreeNode current;
        TreeNode predecessor;
        if (root == null) return res;
        current = root;
        while (current != null) {
            if (current.left == null) {
                res.add(current.val);
                current = current.right;
            } else {
                // find inorder predecessor of current
                predecessor = current.left;
                while (predecessor.right != null && predecessor.right != current) {
                    predecessor = predecessor.right;
                }
                // make current as right child of its inorder predecessor (threading)
                if (predecessor.right == null) {
                    predecessor.right = current; // threading
                    current = current.left;
                } else {
                    // revert threading to restore original tree
                    predecessor.right = null;
                    res.add(current.val);
                    current = current.right;
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        System.out.println("Recursive: " + inorderTraversalRecursive(root));
        System.out.println("Iterative: " + inorderTraversalIterative(root));
        System.out.println("Morris: " + inorderMorrisTraversal(root));
    }
}
