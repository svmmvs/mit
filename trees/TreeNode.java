package trees;

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int val) {
        this.val = val;
    }

    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    static TreeNode search(TreeNode root, int value) {
        if (root == null || root.val == value) {
            return root;
        }

        if (value > root.val) {
            return search(root.right, value);
        } else {
            return search(root.left, value);
        }
    }
}
