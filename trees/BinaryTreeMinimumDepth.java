package trees;

import java.util.ArrayDeque;
import java.util.Deque;

public class BinaryTreeMinimumDepth {
    /*
     * Iterative: number of nodes along shortest path from root down to the nearest leaf
     */
    public static int minDepthBFS(TreeNode root) {
        int res = 0;
        Deque<Pair> queue = new ArrayDeque<>();
        if (root != null) {
            queue.addLast(new Pair(root, 1));
        }
        while (!queue.isEmpty()) {
            Pair temp = queue.removeFirst();
            TreeNode node = temp.node;
            int depth = temp.depth;
            if (node.left == null && node.right == null) {
                res = depth;
                break;
            }
            if (node.left != null) {
                queue.addLast(new Pair(node.left, depth + 1));
            }
            if (node.right != null) {
                queue.addLast(new Pair(node.right, depth + 1));
            }
        }
        return res;
    }

    /*
     * Recursive: number of nodes along shortest path from root down to the nearest leaf
     */
    public static int minDepthDFS(TreeNode root) {
        if (root == null) return 0;
        int left = minDepthDFS(root.left);
        int right = minDepthDFS(root.right);

        if (left == 0 || right == 0) {
            // if both null, returns 1 (only root counted)
            return Math.max(left, right) + 1;
        } else {
            return Math.min(left, right) + 1;
        }
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.left.left.left = new TreeNode(9);
        System.out.println("DFS approach (slow, big left tree, small right tree): " + minDepthDFS(root));
        System.out.println("BFS approach (optimal): " + minDepthBFS(root));
    }
}
