package trees;

import java.util.ArrayDeque;
import java.util.Deque;

public class BinaryTreeMaximumDepth {
    /*
     * Iterative: number of nodes along longest path from root down to the farthest leaf
     */
    public static int maxDepthIterative(TreeNode root) {
        int res = 0;
        Deque<Pair> stack = new ArrayDeque<>();
        if (root != null) {
            stack.addFirst(new Pair(root, 1));
        }
        while (!stack.isEmpty()) {
            Pair temp = stack.removeFirst();
            TreeNode node = temp.node;
            int depth = temp.depth;
            res = Math.max(res, depth);

            if (node.left != null) {
                stack.addFirst(new Pair(node.left, depth + 1));
            }
            if (node.right != null) {
                stack.addFirst(new Pair(node.right, depth + 1));
            }
        }
        return res;
    }

    /*
     * Recursive: number of nodes along longest path from root down to the farthest leaf
     */
    public static int maxDepthRecursive(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = maxDepthRecursive(root.left);
        int right = maxDepthRecursive(root.right);
        return Math.max(left, right) + 1;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.left.left.left = new TreeNode(9);
        System.out.println("Recursive: " + maxDepthRecursive(root));
        System.out.println("Iterative: " + maxDepthIterative(root));
    }
}
