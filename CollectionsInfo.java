import java.util.*;

@SuppressWarnings({"MismatchedQueryAndUpdateOfCollection", "ResultOfMethodCallIgnored"})
public class CollectionsInfo {

    public static void main(String[] args) {
        info();
    }

    public static void info() {
        // List Interface

        /*
         * ArrayList: size dynamically grows and shrinks on add or remove
         */
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1); // O(1), but O(n) when create new and copy over
        arrayList.add(1, 2); // O(n), elements have to be shifted
        arrayList.get(0); // O(1)
        arrayList.remove(0); // O(n), go through entire array and find removal
        arrayList.indexOf(1); // O(n)
        arrayList.contains(1); // O(n)

        /*
         * LinkedList: every element is separate object with data and pointers (node)
         */
        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(1); // O(1) add to tail
        linkedList.add(1, 2); // O(n)
        linkedList.get(0); // O(n)
        linkedList.remove(Integer.valueOf(2)); // O(1) update pointers and done
        linkedList.remove(0); // O(n) find index first
        linkedList.contains(1); // O(n) find index first

        /*
         * Vector: synchronized ArrayList
         */
        Vector<Integer> vector = new Vector<>();

        /*
         * Stack: extends Vector (synchronized), only last in first out (deprecated)
         */
        Stack<String> stacks = new Stack<>();
        stacks.push("");
        stacks.pop();

        // Queue Interface

        /*
         * PriorityQueue: first in first out, natural ordering (ascending), or ordering by comparator
         */
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        // Deque Interface

        /*
         * ArrayDeque: double-ended queue, insert at head or tail, no indices
         */
        Deque<Integer> queue = new ArrayDeque<>(10); // FIRST IN FIRST OUT
        queue.addLast(1);
        queue.removeFirst();
        queue.removeFirstOccurrence(1); // remove interior elements
        queue.removeLastOccurrence(1);

        Deque<Integer> stack = new ArrayDeque<>(10); // LAST IN FIRST OUT
        stack.addFirst(1);
        stack.removeFirst();

        // Set Interface

        /*
         * HashSet: unordered, no duplicates, insert based on hash code, all operations O(1)
         */
        HashSet<String> hashSet = new HashSet<>();

        /*
         * LinkedHashSet: double linked list to store and order, retain order of insertion, all operations O(1)
         */
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

        // SortedSet Interface

        /*
         * TreeSet: no duplicates, use tree for storage, natural ordering ascending, all operations O(log(n))
         */
        TreeSet<String> treeSet = new TreeSet<>();

        // Map Interface

        /*
         * HashMap: key-value pair, order by hash code (no order), all operations O(1)
         */
        HashMap<Integer, String> hashMap = new HashMap<>();

        /*
         * LinkedHashMap: double linked list to store and order, retain order of insertion, all operations O(1)
         */
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();

        /*
         * TreeMap: map sorted by natural ascending ordering of keys or comparator, all operations O(log(n)) by balanced tree
         */
        TreeMap<Integer, String> treeMap = new TreeMap<>();
    }
}
