package sorts;

import java.util.Arrays;

/*
 * swap and heapify, n iterations -> O(n*log(n))
 * auxiliary space complexity: O(1)
 */
public class HeapSort {

    static void heapSort(int[] arr) {
        int n = arr.length;
        // build a heap
        for (int i = n / 2 - 1; i >= 0; i--) {
            heapify(arr, n, i);
        }
        // extract top element and put end to top
        for (int i = n - 1; i > 0; i--) {
            int temp = arr[0];
            arr[0] = arr[i];
            arr[i] = temp;
            heapify(arr, i, 0);
        }
    }

    static void heapify(int[] arr, int n, int i) {
        int largest = i;
        int l = 2 * i + 1;
        int r = 2 * i + 2;
        // if left child is larger
        if (l < n && arr[l] > arr[largest]) {
            largest = l;
        }
        // if right is larger
        if (r < n && arr[r] > arr[largest]) {
            largest = r;
        }
        // if largest is not root, recursively heapify subtree
        if (largest != i) {
            int swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            heapify(arr, n, largest);
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 2};
        heapSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
