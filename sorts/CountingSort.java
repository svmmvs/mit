package sorts;

import java.util.Arrays;

/*
 * O(n + k) time and space complexity, pseudo-polynomial
 */
public class CountingSort {

    static void countSort(int[] arr) {
        if (arr.length < 2) return;
        int max = Arrays.stream(arr).max().getAsInt();
        int min = Arrays.stream(arr).min().getAsInt();
        int range = max - min + 1;
        int[] count = new int[range];
        int[] output = new int[arr.length];

        // store count of every int
        for (int j : arr) {
            count[j - min]++;
        }
        // sum up counts to get actual position of int in output
        for (int i = 1; i < count.length; i++) {
            count[i] += count[i - 1];
        }
        // fill in output from end to start and decrease position counter
        for (int i = arr.length - 1; i >= 0; i--) {
            output[count[arr[i] - min] - 1] = arr[i];
            count[arr[i] - min]--;
        }
        // arr is output (sorted)
        for (int i = 0; i < arr.length; i++) {
            arr[i] = output[i];
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 22};
        countSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
