package sorts;

import java.util.Arrays;

/*
 * sort unsorted array only using reverse array from 0 to i operation
 * O(n) flip operations, worst case: O(n^2),
 * best case: (sorted, no flips) O(n),
 * auxiliary space: O(n)
 */
public class PancakeSort {

    static int[] pancakeSort(int[] arr) {
        int len = arr.length;
        // reduce current size by one
        for (int current = len; current > 1; current--) {
            int maxi = findMaxIndex(arr, current);
            // move maximum element to end
            if (maxi != current - 1) {
                // first move maximum to start
                flip(arr, maxi);
                // then to end by reversing
                flip(arr, current - 1);
            }
        }
        return arr;
    }

    static void flip(int[] arr, int k) {
        int temp;
        int start = 0;
        while (start < k) {
            temp = arr[start];
            arr[start] = arr[k];
            arr[k] = temp;
            start++;
            k--;
        }
    }

    static int findMaxIndex(int[] arr, int n) {
        int maxi, i;
        for (maxi = 0, i = 0; i < n; i++) {
            if (arr[i] > arr[maxi]) {
                maxi = i;
            }
        }
        return maxi;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 2};
        System.out.println(Arrays.toString(pancakeSort(arr)));
    }
}
