package sorts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/*
 * absolute value sort (custom comparator):
 * given array of integer, sort according to absolute values;
 * if same value, then negative first
 */
public class AbsoluteValueSort {

    public static void main(String[] args) {
        int[] arr = new int[]{-1, 1, 2, -5, -9, 8};
        System.out.println(Arrays.toString(absSort(arr)));
    }

    public static int[] absSort(int[] arr) {
        CompareAbs compare = new CompareAbs();
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            list.add(i, arr[i]);
        }
        list.sort(compare);
        for (int j = 0; j < arr.length; j++) {
            arr[j] = list.get(j);
        }
        return arr;
    }

    static class CompareAbs implements Comparator<Integer> {
        @Override
        public int compare(Integer n1, Integer n2) {
            if (Math.abs(n1) < Math.abs(n2)) return -1;
            if (Math.abs(n1) > Math.abs(n2)) return 1;
            return n1.compareTo(n2);
        }
    }
}
