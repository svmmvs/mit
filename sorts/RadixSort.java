package sorts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RadixSort {

    static void countSort(int[] arr, int exp) {
        int n = arr.length;
        int[] output = new int[n];
        int[] count = new int[10];

        // store occurrences in count
        for (int j : arr) {
            count[(j / exp) % 10]++; // get last digit by % 10
        }

        // count now contains actual position of this digit in output
        for (int i = 1; i < 10; i++)
            count[i] += count[i - 1];

        for (int i = n - 1; i >= 0; i--) {
            output[count[(arr[i] / exp) % 10] - 1] = arr[i];
            count[(arr[i] / exp) % 10]--;
        }

        for (int i = 0; i < n; i++) {
            arr[i] = output[i];
        }
    }

    public static void radixSort(int[] arr) {
        List<Integer> numbers = new ArrayList<>();
        for (int j : arr) {
            numbers.add(j);
        }
        int m = Collections.max(numbers);
        // going from least significant decimal point to most significant
        // 1 10 100 1000...
        for (int exp = 1; m / exp > 0; exp *= 10)
            countSort(arr, exp);
    }

    public static void main(String[] args) {
        int[] arr = {170, 45, 75, 90, 802, 24, 2, 66};
        radixSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
