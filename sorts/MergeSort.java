package sorts;

import java.util.Arrays;

/*
* T(n) = 2*T(n/2) + O(n)
	    = O(n) + 2*O(n/2) + 4*O(n/4) + 8*O(n/8) + ... + n*O(1) {1 + log(n) steps}
	    = O(n)*(1 + log(n))
	    = O(n*log(n))
* auxiliary space complexity: O(n)
 */
public class MergeSort {
    // main function that sorts arr[l..r] using merge()
    public static void mergeSort(int[] arr, int start, int end) {
        if (start < end) {
            // find the middle point
            int mid = start + (end - start) / 2;
            // sort both halves
            mergeSort(arr, start, mid);
            mergeSort(arr, mid + 1, end);
            // merge sorted halves
            merge(arr, start, mid, end);
        }
    }

    // merges 2 sub arrays of arr[], first arr[l...m], second arr[m+1...r]
    public static void merge(int[] arr, int start, int mid, int end) {
        // find sizes of 2 sub arrays to be merged
        int n1 = mid - start + 1;
        int n2 = end - mid;
        // create temp arrays
        int[] L = new int[n1];
        int[] R = new int[n2];
        // copy data to temp arrays
        for (int i = 0; i < n1; i++) {
            L[i] = arr[start + i];
        }
        for (int j = 0; j < n2; j++) {
            R[j] = arr[mid + 1 + j];
        }
        // merge temp arrays
        int i = 0, j = 0; // initial index 2 sub arrays
        int k = start; // initial index merged array

        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        // copy remaining L[] if any
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }
        // copy remaining R[] if any
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 2};
        mergeSort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}
