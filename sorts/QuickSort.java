package sorts;

import java.util.Arrays;

/*
 * worst case O(n^2)
 * average/best case O(n*log(n))
 * auxiliary space: O(n) || sort one partition after the other: O(log(n))
 */
public class QuickSort {

    static void quickSort(int[] arr, int start, int end) {
        if (start < end) {
            int part = partition(arr, start, end);
            // sort left and right of partition
            quickSort(arr, start, part - 1);
            quickSort(arr, part + 1, end);
        }
    }

    static int partition(int[] arr, int start, int end) {
        int pivot = arr[end];
        int partIndex = start;
        for (int i = start; i < end; i++) {
            // move smaller elements to left of pivot
            if (arr[i] < pivot) {
                swap(arr, partIndex, i);
                partIndex++;
            }
        }
        swap(arr, partIndex, end);
        return partIndex;
    }

    static void swap(int[] arr, int s, int t) {
        int temp = arr[s];
        arr[s] = arr[t];
        arr[t] = temp;
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 2};
        quickSort(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }
}
