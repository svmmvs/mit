package sorts;

import java.util.Arrays;

/*
 * repeatedly swap adjacent elements if in wrong order; need one pass without any swaps in end
 * worst case: O(n^2), best case (if sorted): O(n)
 * auxiliary space: O(1)
 */
public class BubbleSort {

    static void bubbleSort(int[] arr) {
        int i, j, temp;
        boolean swapped;
        for (i = 0; i < arr.length - 1; i++) {
            swapped = false;
            for (j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
            }
            if (!swapped) {
                break;
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = new int[]{4, 9, 1, 8, 5, 2};
        bubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
