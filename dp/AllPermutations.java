package dp;

import java.util.ArrayList;
import java.util.List;

/*
 * n! permutations, leaves of a tree, to get to these leaves, n calls necessary, O(n*n!)
 * auxiliary space O(n!) to store all permutations
 */
public class AllPermutations {

    public static void main(String[] args) {
        int[] arr = new int[]{1, 9, 4};
        System.out.println(permutations(arr));
    }

    public static List<List<Integer>> permutations(int[] arr) {
        List<List<Integer>> list = new ArrayList<>();
        backtrack(list, new ArrayList<>(), arr);
        return list;
    }

    public static void backtrack(List<List<Integer>> list, List<Integer> temp, int[] arr) {
        if (temp.size() == arr.length) {
            list.add(new ArrayList<>(temp));
        }
        for (int j : arr) {
            if (temp.contains(j)) continue;
            temp.add(j);
            backtrack(list, temp, arr);
            temp.remove(temp.size() - 1);
        }
    }
}
