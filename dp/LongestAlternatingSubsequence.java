package dp;

/*
 * Find the longest alternating subsequence picking elements in array (order has to remain)
 * e.g. [1, 11, 9, 22, 24, 26, 19, 99], result: [1, 11, 9, 22/24/26 , 19, 99], length 6
 */
public class LongestAlternatingSubsequence {

    public static void main(String[] args) {
        int[] arr = new int[]{1, 11, 9, 22, 24, 26, 19, 99};
        System.out.println(longestAlternating(arr));
        System.out.println(longestAlternatingOptimized(arr));
    }

    /*
     * No need to store used values in array, instead 2 variables with
     * increasing: longest alternating subsequence ending thus far and current > previous
     * and decreasing: longest alternating subsequence ending thus far and current < previous
     *
     * recursions: increase "increasing" if last element in alternating sequence smaller than previous
     * and increase "decreasing" if last element in alternating sequence greater than previous
     *
     * time complexity: O(n), space complexity: O(1)
     */
    static int longestAlternatingOptimized(int[] array) {
        int n = array.length;
        int increasing = 1;
        int decreasing = 1;

        for (int i = 1; i < n; i++) {
            if (array[i] > array[i - 1]) {
                increasing = decreasing + 1;
            } else if (array[i] < array[i - 1]) {
                decreasing = increasing + 1;
            }
        }

        return Math.max(increasing, decreasing);
    }

    /*
     * Create 2D array with alternate[i][0] longest alternating subsequence ending at i and last > last-1
     * and alternate[i][1] longest alternating subsequence ending at i and last < last-1
     *
     * recursions: alternate[i][0] = maximum(alternate[i][0], alternate[j][1] + 1), j<i and array[j]<array[i]
     * and alternate[i][1] = maximum(alternate[i][1], alternate[j][0] + 1), j<i and array[j]>[array[i]
     *
     * time complexity: O(n^2), space complexity: O(n)
     */
    static int longestAlternating(int[] array) {
        int n = array.length;
        int[][] alternate = new int[n][2];
        int result = 1;

        // init all longest sequences with 1
        for (int i = 0; i < n; i++) {
            alternate[i][0] = alternate[i][1] = 1;
        }

        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (array[j] < array[i] && alternate[i][0] < alternate[j][1] + 1) {
                    alternate[i][0] = alternate[j][1] + 1;
                }
                if (array[j] > array[i] && alternate[i][1] < alternate[j][0] + 1) {
                    alternate[i][1] = alternate[j][0] + 1;
                }
            }

            if (result < Math.max(alternate[i][0], alternate[i][1])) {
                result = Math.max(alternate[i][0], alternate[i][1]);
            }
        }

        return result;
    }
}
