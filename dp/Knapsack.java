package dp;

import java.util.Arrays;

/*
 * maximize sum of values for subset of n items of total size smaller equal knapsack size S
 * sub problems: value for suffix i of items and remaining capacity j, number of subs = O(n*S)
 * guess: include item i or not, number of choices = 2
 * recurrence: dynamic(i,j) = max{dynamic(i+1,j),
 *             dynamic(i+1, j - s_i) + v_i, if j greater equal s_i}
 *             dynamic(n,j) = 0; time per sub problem = O(1)
 * topological order: for i in n,...,0: for j in 0,...,S: total time O(n*S)
 * solution: dynamic(0,S) and use parent pointers to recover subset
 * pseudo-polynomial O(n*S) if S fits in a word
 */
public class Knapsack {

    public static void main(String[] args) {
        // {value, weight}
        int[][] items = new int[][]{{3, 2}, {4, 3}, {7, 5}, {1, 1}};
        int capacity = 6;
        System.out.println(Arrays.deepToString(knapsack(items, capacity)));
    }

    public static int[][] knapsack(int[][] items, int capacity) {
        int N = items.length; // number of items
        int[] dollar = new int[N];
        int[] weight = new int[N];
        for (int n = 0; n < N; n++) {
            dollar[n] = items[n][0];
            weight[n] = items[n][1];
        }

        int[][] dp = new int[N + 1][capacity + 1];
        boolean[][] picks = new boolean[N + 1][capacity + 1];

        for (int i = N; i >= 0; i--) {
            for (int j = 0; j <= capacity; j++) {
                if (i == N) {
                    dp[i][j] = 0; // init
                } else {
                    // do not take item i
                    int nope = dp[i + 1][j];
                    // take item i
                    int yeah = Integer.MIN_VALUE;
                    if (j >= weight[i]) {
                        yeah = dp[i + 1][j - weight[i]] + dollar[i];
                    }
                    // select better option
                    dp[i][j] = Math.max(nope, yeah);
                    picks[i][j] = yeah > nope;
                }
            }
        }

        // determine which items to take
        int[][] res = new int[N][2];
        for (int n = 0, s = capacity; n < N; n++) {
            if (picks[n][s]) {
                res[n][0] = dollar[n];
                res[n][1] = weight[n];
                s = s - weight[n];
            }
        }
        // remove [0, 0] entries
        return Arrays.stream(res).filter(arr -> !Arrays.equals(arr, new int[2])).toArray(int[][]::new);
    }
}
