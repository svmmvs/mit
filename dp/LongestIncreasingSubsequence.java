package dp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

// given an integer array, return the length of the longest strictly increasing subsequence
public class LongestIncreasingSubsequence {

    public static void main(String[] args) {
        int[] arr = new int[]{2, 5, 6, 3, 1};
        System.out.println("dynamic: " + lengthOfLongestDynamic(arr));
        System.out.println(lengthOfLongest(arr));
        System.out.println(lengthOfLongestElegant(arr));
    }

    /* dynamic programming solution with O(n^2) time
    	start at number i from 0 to n-1
         for i = n-1 to 0
            choices = [1]
            for j in i+1 to n
                if a[j] > a[i]
                   choices.add(dp[j]+1)
            dp[i] = max(choices)
         finally: take max of all dp entries
     */
    public static int lengthOfLongestDynamic(int[] numbers) {
        int res = 0;
        int len = numbers.length;
        int[] dp = new int[len + 1];
        dp[len] = 0;

        for (int i = len - 1; i >= 0; i--) {
            List<Integer> choices = new ArrayList<>();
            choices.add(1);
            for (int j = i + 1; j < len; j++) {
                if (numbers[j] > numbers[i]) {
                    choices.add(dp[j] + 1);
                }
            }
            dp[i] = Collections.max(choices);
        }

        for (int i : dp) {
            if (i > res) {
                res = i;
            }
        }
        return res;
    }

    // optimal solution with tree set ceiling method O(n*log(n))
    public static int lengthOfLongestElegant(int[] numbers) {
        TreeSet<Integer> set = new TreeSet<>();
        for (int k : numbers) {
            Integer ceil = set.ceiling(k);
            if (ceil != null) {
                // equivalent to replacing first or ceil element
                set.remove(ceil);
            }
            set.add(k);
        }
        return set.size();
    }

    // optimal solution by collecting last element of all possible sequences and updating
    static int lengthOfLongest(int[] numbers) {
        int[] tailTable = new int[numbers.length];
        int size = 1;
        tailTable[0] = numbers[0];
        for (int curr : numbers) {
            if (curr < tailTable[0]) {
                // current is new smallest value
                tailTable[0] = curr;
            } else if (curr > tailTable[size - 1]) {
                // current extends largest subsequence
                tailTable[size++] = curr;
            } else {
                // current replaces smallest larger element
                // current is new end candidate of a subsequence
                int start = 0;
                int end = size;
                while (start != end) {
                    int m = (start + end) / 2;
                    if (tailTable[m] >= curr) end = m;
                    else start = m + 1;
                }
                tailTable[end] = curr;
            }
        }
        return size;
    }
}
