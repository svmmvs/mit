package dp;

import java.util.Arrays;

/*
 * find subset of items who(se weights) add up to capacity S
 */
public class KnapsackSubset {

    public static void main(String[] args) {
        int[] items = new int[]{2, 3, 8, 1, 18};
        int capacity = 6;
        System.out.println(subset(items, capacity));
    }

    public static boolean subset(int[] items, int S) {
        int N = items.length;
        boolean[][] dp = new boolean[N + 1][S + 1];

        for (int i = N; i >= 0; i--) {
            for (int j = 0; j <= S; j++) {
                if (i == N) {
                    dp[i][j] = j == 0;
                } else {
                    // do not take item i
                    dp[i][j] = dp[i + 1][j];
                    // take item i
                    if (j >= items[i]) {
                        dp[i][j] = (dp[i][j] || dp[i + 1][j - items[i]]);
                    }
                }
            }
        }
        if (dp[0][S]) {
            int[] picks = new int[N];
            for (int n = 0, s = S; n < N; n++) {
                if (dp[n][s] && s >= items[n]) {
                    picks[n] = items[n];
                    s = s - items[n];
                }
            }
            int[] filterPicks = Arrays.stream(picks).filter(i -> i != 0).toArray();
            System.out.println(Arrays.toString(filterPicks));
        }
        return dp[0][S];
    }
}
