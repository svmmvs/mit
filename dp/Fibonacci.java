package dp;

public class Fibonacci {

    public static void main(String[] args) {
        int k = 10;
        System.out.println("Recursive: " + fibonacci(k));
        System.out.println("Top-Down: " + fibonacciMemo(k));
        System.out.println("Bottom-Up: " + fibonacciDP(k));
        System.out.println("Bottom-Up Optimized: " + fibonacciDPOptimized(k));
    }

    private static int fibonacciDP(int i) {
        if (i == 0) return 0;
        else if (i == 1) return 1;
        int[] dp = new int[i];
        dp[0] = 0;
        dp[1] = 1;
        for (int j = 2; j < i; j++) {
            dp[j] = dp[j - 1] + dp[j - 2];
        }
        return dp[i - 1] + dp[i - 2];
    }

    private static int fibonacciDPOptimized(int i) {
        if (i == 0) return 0;
        int a = 0;
        int b = 1;
        for (int j = 2; j < i; j++) {
            int c = a + b;
            a = b;
            b = c;
        }
        return a + b;
    }

    // O(2*n) = O(n)
    private static int fibonacciMemo(int i) {
        return fibonacciMemory(i, new int[i + 1]);
    }

    private static int fibonacciMemory(int i, int[] memo) {
        if (i == 0 || i == 1) return i;
        if (memo[i] == 0) {
            memo[i] = fibonacciMemory(i - 1, memo) + fibonacciMemory(i - 2, memo);
        }
        return memo[i];
    }

    // O(1.6^n) exponential
    private static int fibonacci(int i) {
        if (i == 0) return 0;
        if (i == 1) return 1;
        return fibonacci(i - 1) + fibonacci(i - 2);
    }
}
