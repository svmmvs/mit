package dp;

import java.util.ArrayList;
import java.util.List;

/* integer array of unique elements, find all possible subsets (no duplicate subset)
 * recursive function is called 2^n times: 2 choices at each iteration in numbers array
 * either include numbers[i] in the current set or exclude numbers[i]

 * need copy of the current set because reuse the original one to build all the valid subsets
 * this copy costs O(n) and is performed at each call of the recursive function (2^n times)
 * O(n*(2^n))
 */
public class AllSubsets {
    public static void main(String[] args) {
        int[] arr = new int[]{1, 9, 4};
        System.out.println(subsets(arr));
    }

    public static List<List<Integer>> subsets(int[] numbers) {
        List<List<Integer>> list = new ArrayList<>();
        backtrack(list, new ArrayList<>(), numbers, 0);
        return list;
    }

    private static void backtrack(List<List<Integer>> list, List<Integer> tempList, int[] numbers, int start) {
        list.add(new ArrayList<>(tempList));
        for (int i = start; i < numbers.length; i++) {
            tempList.add(numbers[i]);
            backtrack(list, tempList, numbers, i + 1);
            tempList.remove(tempList.size() - 1);
        }
    }
}
