package dp;

/*
 * encode letter A -> 1, B -> 2, C -> 3,...,Z -> 26 english letters
 * input string of digits S from 0-9 representing an encoded message,
 * return the number of ways to decode
 */
public class DecodingWays {
    /*
    - sub problems: number of ways on three character string suffix[i,...,i+2] number of sub problems O(n)
    - guess: number of choices = 4, either start 0, start 1, 2 or else
    - recurrence dynamic(i) = 0 if char is 0 (no way to decode)
                            = dynamic(i+1) if char is 3,...,9 (adding 3 to 9 does not give more ways to decode)
                            = dynamic(i+1) + dynamic(i+2) if char is 1 (all combos from 1 + (rest) and from 1X + (rest+1))
                            = dynamic(i+1) + dynamic(i+2)||0 if char is 2 (all combos from 2 + (rest) and 2X + (rest+1) if 2X in 26)
    - topological order: for n-1...0
    - solution: dynamic(0) (whole string from i=0...n)
    */
    public static int decodeVariations(String S) {
        int len = S.length();
        int[] dynamic = new int[len + 2];
        dynamic[len] = 1;

        for (int i = len - 1; i >= 0; i--) {
            char current = S.charAt(i);
            if (current == '0') {
                dynamic[i] = 0;
            } else if (current == '1') {
                dynamic[i] = dynamic[i + 1] + dynamic[i + 2];
            } else if (current == '2') {
                dynamic[i] = dynamic[i + 1];
                if (i + 1 < len && S.charAt(i + 1) <= '6') {
                    dynamic[i] += dynamic[i + 2];
                }
            } else {
                dynamic[i] = dynamic[i + 1];
            }
        }
        return dynamic[0];
    }

    public static void main(String[] args) {
        String s = "1226"; // 1 2 2 6 || 1 22 6 || 1 2 26 || 12 2 6 || 12 26
        System.out.println(decodeVariations(s));
    }
}
