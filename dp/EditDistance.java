package dp;

/*
 * given 2 strings return minimum number of operations to convert one string to the other
 * operations: insert, delete, replace character
 */
public class EditDistance {

    public static void main(String[] args) {
        String s1 = "execution";
        String s2 = "intention";
        System.out.println("recursive: " + editDistRecursive(s1, s2));
        System.out.println(editDist(s1, s2));
    }

    // O(n*m) string lengths
    public static int editDist(String s1, String s2) {
        int n = s1.length();
        int m = s2.length();
        int[][] dynamics = new int[n + 1][m + 1];

        // empty string 1, string 2 starting from empty string to full string (0 removes -> m removes to get empty)
        for (int j = m; j >= 0; j--) {
            dynamics[n][j] = m - j;
        }

        // empty string 2, string 1 starting from empty string to full string (0 removes -> n removes to get empty)
        for (int i = n; i >= 0; i--) {
            dynamics[i][m] = n - i;
        }

        for (int i = n - 1; i >= 0; i--) {
            for (int j = m - 1; j >= 0; j--) {
                // if characters same, add nothing
                if (s1.charAt(i) == s2.charAt(j)) {
                    dynamics[i][j] = dynamics[i + 1][j + 1];
                } else {
                    // else get minimum of 3 operations
                    dynamics[i][j] = 1 + Math.min(dynamics[i + 1][j + 1], // replace
                            Math.min(dynamics[i][j + 1]  // remove
                                    , dynamics[i + 1][j]));   // insert
                }
            }
        }

        return dynamics[0][0];
    }

    // exponential O(3^n) (3 operations)
    public static int editDistRecursive(String s1, String s2) {
        // if one string empty, need remaining length insert operations
        if (s1.length() == 0) {
            return s2.length();
        }
        if (s2.length() == 0) {
            return s1.length();
        }
        String remaining1 = s1.substring(1);
        String remaining2 = s2.substring(1);
        int res;

        if (s1.charAt(0) == s2.charAt(0)) {
            // match, no operation needed, next
            res = editDistRecursive(remaining1, remaining1);
        } else {
            // no match: add 1 operation and recur for the remaining Strings
            res = 1 + Math.min(editDistRecursive(remaining1, remaining2), Math.min(editDistRecursive(remaining1, s2), editDistRecursive(s1, remaining2)));
        }
        return res;
    }
}
