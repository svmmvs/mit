package dp;

/*
 * given integer array and target integer, find all combinations that sum up to target, can repeat integer
 */
public class TargetSumCombos {
    public static void main(String[] args) {
        int[] items = new int[]{2, 3, 8, 1};
        int target = 6; // 222 33 111111 231 21111 2211 3111
        System.out.println(findAllCombinations(target, items));
    }

    /*
    sub problems: find all combinations starting from last element coins[n-1], coins[n-1...n-2], ... coins[n-1...0]
    guess: take the coin or not, we do not know, guess
    recurrence: dp[i][j] = all combinations to get amount
                do not take coin = dp[i][j]
                take coin = if (j >= coins[i]) dp[i+1][j - coins[i]]
                take same coin = if (j >= coins[i]) dp[i][j - coins[i]]
    topological order: n length, i from (n to 0), n special case
                                    j from (0 to amount)
                                        start: dp[n][j...] = 0, dp[i...][0] = 1
    solution: dp[0][amount]
    O(n * amount)
    */
    public static int findAllCombinations(int amount, int[] coins) {
        int N = coins.length;
        int[][] dp = new int[N + 1][amount + 1];

        for (int i = N; i >= 0; i--) {
            for (int j = 0; j <= amount; j++) {
                if (i == N) {
                    dp[i][j] = 0;
                } else if (j == 0) {
                    dp[i][j] = 1;
                } else {
                    // do not take coin
                    int nope = dp[i + 1][j];
                    // take coin
                    int yeah = 0;
                    if (j >= coins[i]) {
                        yeah = dp[i][j - coins[i]];
                    }
                    dp[i][j] = nope + yeah;
                }
            }
        }
        return dp[0][amount];
    }
}
