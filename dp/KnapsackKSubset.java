package dp;

import java.util.Arrays;

/*
 * find exactly k items, who(se weights) add up to S
 */
public class KnapsackKSubset {

    public static void main(String[] args) {
        int[] items = new int[]{2, 3, 8, 1, 18};
        int capacity = 28;
        int K = 3;
        System.out.println(kSubset(items, capacity, K));
    }

    public static boolean kSubset(int[] items, int S, int K) {
        int N = items.length;
        boolean[][][] dp = new boolean[N + 1][S + 1][K + 1];

        for (int i = N; i >= 0; i--) {
            for (int j = 0; j <= S; j++) {
                for (int k = 0; k <= K; k++) {
                    if (i == N) {
                        dp[i][j][k] = j == 0 && k == 0;
                    } else {
                        // do not take item i
                        dp[i][j][k] = dp[i + 1][j][k];
                        // take item i
                        if (j >= items[i] && k > 0) {
                            dp[i][j][k] = (dp[i][j][k] || dp[i + 1][j - items[i]][k - 1]);
                        }
                    }
                }
            }
        }

        if (dp[0][S][K]) {
            int[] picks = new int[N];
            for (int n = 0, s = S, k = K; n < N; n++) {
                if (dp[n][s][k] && s >= items[n] && dp[n + 1][s - items[n]][k - 1]) {
                    picks[n] = items[n];
                    s = s - items[n];
                    k--;
                }
            }
            int[] filterPicks = Arrays.stream(picks).filter(i -> i != 0).toArray();
            System.out.println(Arrays.toString(filterPicks));
        }
        return dp[0][S][K];
    }
}
