package dp;

/*
 * partition of integer array into k equal subsets
 * auxiliary space: O(n) height of recursion tree;
 * time O(k*2^n), 2 recursive layers, inner layer 2^n, outer layer k
 */
public class EqualKSubsetSum {

    public static void main(String[] args) {
        int[] items = new int[]{7, 3, 8, 1, 5};
        System.out.println("can partition: " + canPartitionKSubsets(items, 3));
    }

    public static boolean canPartitionKSubsets(int[] numbers, int k) {
        int sum = 0;
        int max = 0;
        for (int num : numbers) {
            sum += num;
            max = Math.max(max, num);
        }
        if (k <= 0 || sum % k != 0) {
            return false;
        }
        if (max > sum / k) {
            return false;
        }

        boolean[] visited = new boolean[numbers.length];
        return canPartition(numbers, visited, 0, k, 0, sum / k);
    }

    public static boolean canPartition(int[] numbers, boolean[] visited, int start, int k, int currentSum, int target) {
        if (k == 1) {
            return true;
        }

        if (currentSum == target) {
            return canPartition(numbers, visited, 0, k - 1, 0, target);
        }

        for (int i = start; i < numbers.length; i++) {
            if (!visited[i]) {
                visited[i] = true;
                if (canPartition(numbers, visited, i + 1, k, currentSum + numbers[i], target)) {
                    return true;
                }
                visited[i] = false;
            }
        }
        return false;
    }
}
