package dp;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

// distinct positive integer array, return any largest subset which satisfies:
// array[i] % array[j] == 0 or array[j] % array[i] == 0
public class LargestDivisibleSubset {

    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3, 6, 36, 64, 128, 256};
        System.out.println(largestDivisibleSubset(arr));
    }

    // O(n^2) time and O(n) space
    static List<Integer> largestDivisibleSubset(int[] array) {
        int n = array.length;
        int[] dp = new int[n];
        Arrays.fill(dp, 1);

        // sort array to be able to modulo check and to add up number of divisors in dp
        Arrays.sort(array);
        // get size (number of divisible) of the largest divisible subset
        int size = 1;
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < i; j++) {
                if (array[i] % array[j] == 0) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                    size = Math.max(size, dp[i]);
                }
            }
        }

        // determine content of largest divisible subset
        int previous = -1;
        // linked list to return ascending list
        LinkedList<Integer> subset = new LinkedList<>();

        for (int i = n - 1; i >= 0; i--) {
            // previous fulfills -1 condition at start, then will be used to check divisible condition (modulo)
            if (dp[i] == size && (previous == -1 || previous % array[i] == 0)) {
                subset.addFirst(array[i]);
                size--;
                previous = array[i];
            }
        }

        return subset;
    }
}
