package dp;

/*
 * array with positive integers,
 * find two subsets partition such that the sum of elements in both subsets is equal
 */
public class EqualSubsetSum {

    public static void main(String[] args) {
        int[] items = new int[]{6, 3, 8, 1, 18};
        System.out.println("can partition: " + canPartition(items));
        System.out.println("O(n) space: " + canPartitionOptimalSpace(items));
    }

    public static boolean canPartition(int[] numbers) {
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        // odd number cannot be divided
        if ((sum & 1) == 1) {
            return false;
        }
        sum /= 2;
        int n = numbers.length;
        boolean[][] dp = new boolean[n + 1][sum + 1];

        for (int i = n; i >= 0; i--) {
            for (int j = 0; j <= sum; j++) {
                // no number in knapsack
                if (i == n) {
                    // can only achieve sum 0
                    dp[i][j] = j == 0;
                } else {
                    // do not take number i
                    boolean nope = dp[i + 1][j];
                    // take number i
                    boolean yeah = false;
                    if (j >= numbers[i]) {
                        yeah = dp[i + 1][j - numbers[i]];
                    }
                    dp[i][j] = (nope || yeah);
                }
            }
        }
        return dp[0][sum];
    }

    // solution with one dimensional array (O(n) auxiliary space)
    public static boolean canPartitionOptimalSpace(int[] numbers) {
        int sum = 0;
        for (int num : numbers) {
            sum += num;
        }
        // odd number cannot be divided
        if ((sum & 1) == 1) {
            return false;
        }
        sum /= 2;
        boolean[] dp = new boolean[sum + 1];
        dp[0] = true;

        for (int num : numbers) {
            for (int j = sum; j > 0; j--) {
                // do not take number
                boolean nope = dp[j];
                // take number
                boolean yeah = false;
                if (j >= num) {
                    yeah = dp[j - num];
                }
                dp[j] = (nope || yeah);
            }
        }
        return dp[sum];
    }
}
