package search;

// k-smallest number in array with quick select (O(n) time on average, O(n^2) worst case)
public class KSmallestNumber {

    public static int pivot(int[] arr, int left, int right) {
        int pivot = arr[left];
        while (left < right) {
            while (left < right && arr[right] >= pivot) {
                right--;
            }
            arr[left] = arr[right];
            while (left < right && arr[left] <= pivot) {
                left++;
            }
            arr[right] = arr[left];
        }
        arr[left] = pivot;
        return left;
    }

    public static int kthSmallest(int[] arr, int k) {
        int len = arr.length;
        int left = 0;
        int right = len - 1;
        while (left <= right) {
            int mid = pivot(arr, left, right);
            if (mid == k) break;
            if (mid < k) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return arr[k - 1];
    }

    public static void main(String[] args) {
        int[] array = new int[]{10, 4, 5, 8, 6, 11, 26};
        int k = 3;
        System.out.println(kthSmallest(array, k));
    }
}
