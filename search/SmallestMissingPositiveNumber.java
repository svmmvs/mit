package search;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * Find the smallest missing positive integer in unsorted array
 */
public class SmallestMissingPositiveNumber {

    // time and space complexity O(n) with hash set helper
    public static int firstMissingPositiveWithSet(int[] array) {
        Set<Integer> distinct = Arrays.stream(array).boxed().collect(Collectors.toSet());
        int index = 1;
        while (true) {
            if (!distinct.contains(index)) {
                return index;
            } else {
                index++;
            }
        }
    }

    // time complexity O(n) and space complexity O(1)
    public static int firstMissingPositive(int[] array) {
        int k = partitionOnceAtZero(array);

        // 1 Missing integer is in range 1 to k
        for (int i = 0; i < k; i++) {

            /* Fix negative marking from previous loop steps:
             * array[i] was all positive at start, but abused for marking to achieve O(1) space
             * so array[i] can be negative during loop process
             * and needs to be made positive because it is used as array index
             */
            int val = Math.abs(array[i]);

            /* Two conditions to check before marking the abused index as negative:
             * 1. Integer has to be smaller than k, because we are only interested in positive numbers
             * 2. Integer has to be checked on positivity, because we do not want to mark a negative entry as negative,
             * which would neutralize our previous marking
             *
             * Mark the integer at the interested index as negative for the last step
             */
            if (val - 1 < k && array[val - 1] >= 0) {
                array[val - 1] = -array[val - 1];
            }
        }

        /* Last step to check for the first positive entry, which means it was not marked as negative,
         * which means it was not present in array
         */
        for (int i = 0; i < k; i++) {
            if (array[i] > 0) {
                return i + 1;
            }
        }

        // 2 Integers from 1 to k are in array, the smallest positive integer is k+1
        return k + 1;
    }

    /* Quicksort with pivot element 0 and a single partition
     * to change array structure to [positive numbers, 0, negative numbers]
     */
    private static int partitionOnceAtZero(int[] array) {
        final int pivot = 0;
        int index = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > pivot) {
                swap(array, i, index);
                index++;
            }
        }

        // index of first negative integer
        return index;
    }

    private static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public static void main(String[] args) {
        int[] array = new int[]{4, 4, -10, 10, 20};
        System.out.println("O(n) space: " + firstMissingPositiveWithSet(array));
        System.out.println("O(1) space: " + firstMissingPositive(array));
    }
}
