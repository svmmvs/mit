package search;

import java.util.Arrays;

/*
two arrays with same length (not sorted integers), target integer
find combination of two arrays entries that have a sum closest to target
example:
array1 = [-1, 3, 8, 2, 9, 5]
array2 = [4, 1, 2, 10, 5, 20]
target = 24
return (5, 20) or (3, 20)
*/
public class ClosestSumToTarget {

    public static void main(String[] args) {
        int[] array1 = new int[]{1, 3, 8, 2, 9, 5};
        int[] array2 = new int[]{4, 1, 2, 10, 5, 20};
        int target = 24;
        System.out.println(Arrays.toString(closestPair(array1, array2, target)));
    }

    // O(n*log(n))
    public static int[] closestPair(int[] arr1, int[] arr2, int target) {
        int[] res = new int[2];
        if (arr1.length == 0 || arr2.length == 0) return res;

        // sort to apply binary search
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        int difference = Integer.MAX_VALUE;
        // indices of arrays
        int res1 = 0;
        int res2 = 0;
        // indices of loop
        int start = 0;
        int end = arr1.length - 1;

        while (start < end) {
            // if closer, update res1 res2 and (minimize) difference
            if (Math.abs(arr1[start] + arr2[end] - target) < difference) {
                difference = Math.abs(arr1[start] + arr2[end] - target);
                res1 = start;
                res2 = end;
            }

            if (arr1[start] + arr2[end] > target) {
                end--;
            } else {
                start++;
            }
        }

        res[0] = arr1[res1];
        res[1] = arr2[res2];
        return res;
    }
}
