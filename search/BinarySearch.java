package search;

public class BinarySearch {

    static int binarySearch(int[] arr, int search) {
        int result = -1;
        int start = 0;
        int end = arr.length - 1;
        int mid = (start + end) / 2;

        while (start <= end) {
            if (arr[mid] < search) {
                start = mid + 1;
            } else if (arr[mid] == search) {
                result = mid;
                break;
            } else {
                end = mid - 1;
            }
            mid = (start + end) / 2;
        }
        return result;
    }

    static int binarySearchRecursive(int[] arr, int start, int end, int search) {
        if (start <= end) {
            int mid = start + (end - start) / 2;
            if (arr[mid] == search) {
                return mid;
            }
            if (arr[mid] > search) {
                return binarySearchRecursive(arr, start, mid - 1, search);
            } else {
                return binarySearchRecursive(arr, mid + 1, end, search);
            }
        }
        return -1; // not found
    }

    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 4, 5, 8, 9};
        System.out.println(binarySearch(arr, 8));
        System.out.println(binarySearch(arr, 2));
        System.out.println("Recursive: " + binarySearchRecursive(arr, 0, arr.length, 2));
    }
}
