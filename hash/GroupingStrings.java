package hash;

import java.util.*;

/*
 * Array of strings, group anagrams together in any order
 * Anagram: word formed by rearranging letters of another word using letters exactly once
 */
public class GroupingStrings {

    // with sorting to get keys and compare strings (time complexity O(m*n*log(n)))
    static List<List<String>> groupStringsBySort(String[] strings) {
        if (strings == null || strings.length == 0) {
            return new ArrayList<>();
        }

        Map<String, List<String>> map = new HashMap<>();
        for (String string : strings) {
            char[] chars = string.toCharArray();
            Arrays.sort(chars);
            String key = String.valueOf(chars);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }
            map.get(key).add(string);
        }
        return new ArrayList<>(map.values());
    }

    // without sorting using char[26] to count frequencies of letters in strings (time complexity O(m*n))
    static List<List<String>> groupStrings(String[] strings) {
        if (strings == null || strings.length == 0) {
            return new ArrayList<>();
        }

        Map<String, List<String>> map = new HashMap<>();
        for (String string : strings) {
            char[] frequency = new char[26];
            for (char c : string.toCharArray()) {
                frequency[c - 'a']++;
            }
            String key = String.valueOf(frequency);
            if (!map.containsKey(key)) {
                map.put(key, new ArrayList<>());
            }
            map.get(key).add(string);
        }
        return new ArrayList<>(map.values());
    }

    public static void main(String[] args) {
        String[] search = new String[]{"hello", "olleh", "jaywalk", "walkjay", "worldz"};
        System.out.println(groupStringsBySort(search));
        System.out.println(groupStrings(search));
    }
}
