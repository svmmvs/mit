package hash;

/*
 * given 2 strings s and t, does s occur as substring of t?
 * compare each substring: O(|s|*|t|) slow
 * rolling hash: O(|s| + |t|*cost of hash function)
 */
public class KarpRabin {

    static int findSubString(String text, String wanted) {
        int windowLen = wanted.length();
        int docLen = text.length();
        // number of matches;
        int counter = 0;
       /* choose p as prime close to words size (2^32 on 32-bit machine),
          around 4 billion, choose first prime > 1 billion */
        long p = 10000000007L;
        int base = 256;
       /* int invBase = base^(-1) % p;
          long magic = base^windowLen % p;
          precompute base window length constant for remove usage:
           magic = magic/base mod p {multiplicative inverse on primes}
                 = magic*invBase mod p
                 = base^(windowLen - 1) mod p;
       */
        long magic = 1;
        for (int i = 1; i < windowLen; i++) {
            magic = (base * magic) % p;
        }
        // init first windows;
        long wantedHash = 0;
        long hash = 0;
        for (int i = 0; i < windowLen; i++) {
            wantedHash = (wantedHash * base + wanted.charAt(i)) % p;
            hash = (hash * base + text.charAt(i)) % p;
        }

        // same hash value h_k == h_w does not mean same string k == w
        if (hash == wantedHash) {
            if (text.substring(0, windowLen - 1).equals(wanted)) {
                counter++;
            }
        }

        // slide window if hashes do not match, else compare strings
        for (int s = windowLen; s < docLen; s++) {
            int oldCharacter = text.charAt(s - windowLen);
            int newCharacter = text.charAt(s);
            hash = hashNextSlide(hash, oldCharacter, newCharacter, base, p, magic);

            if (hash == wantedHash) {
                if (text.substring(s - windowLen + 1, s + 1).equals(wanted)) {
                    counter++;
                }
            }
        }
        return counter;
    }

    static long hashNextSlide(long oldHash, int oldChar, int newChar, int base, long p, long magic) {
        long rollingHash;
       /* hash() h(k) = k mod p, but cannot mod big number;
          newBigNum = (oldBigNum-old*base^(windowLen-1))*base + newVal
                     = oldBigNum*base - old*magic*base + newVal
          hash(newBigNum) = [oldBigNum*base - old*magic*base + newVal] mod p
                           = [hash(oldBigNum)*base - old*magic*base + newVal] mod p

          skip a char - move sliding window by 1 and delete the oldest char
          add a char - move sliding window by 1 and add new char
          remove(oldVal) {
            hash(newBigNum) = [hash(oldBigNum) - old*magic + p*base] mod p
          }
          add(newVal) {
            hash(newBigNum) = [hash(oldBigNum){after remove}*base + newVal] mod p
          }
       */
        rollingHash = (oldHash * base - oldChar * magic * base + newChar) % p;
        return rollingHash;
    }

    public static void main(String[] args) {
        String text = "This is a test for finding a substring.";
        String search = "a";
        System.out.println(findSubString(text, search));
    }
}
