package hash;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapsAndSets {

    static void setupMapsAndSets() {
        Map<Integer, String> mine = new HashMap<>();
        mine.put(1, "good"); // add element
        mine.replace(1, "bad"); // change element
        mine.remove(1); // remove element
        mine.clear(); // remove all elements
        mine.put(1, "good");
        mine.put(2, "bad");
        boolean oneKey = mine.containsKey(1); // is key in map
        boolean goodValue = mine.containsValue("good"); // is value in map

        Set<Integer> keys = mine.keySet(); // get set of keys
        Collection<String> values = mine.values(); // get collection of values

        // iterate through all entries
        mine.forEach((key, value) -> {
            int myKey = key + 1;
            String myValue = value + "";
        });
    }

    public static void main(String[] args) {
        setupMapsAndSets();
    }
}
