package hash;

import java.util.HashSet;
import java.util.Set;

public class DistinctEchoSubstrings {
    static long p = 10000000007L;
    static int base = 26;

    static int distinctEchoSubstrings(String s) {
        int textLen = s.length();
        char[] charText = s.toCharArray();
        Set<Long> results = new HashSet<>();
        long magic = 1; // 26^0
        for (int t = 1; 2 * t - 1 < textLen; t++) {
            magic = (base * magic) % p; // base window length constant (magic) * base
            long hash1 = initHash(charText, 0, t - 1);
            long hash2 = initHash(charText, t, 2 * t - 1);
            if (hash1 == hash2 && stringsMatch(charText, 0, t)) {
                results.add(hash1);
            }
            for (int i = 1; i + 2 * t - 1 < textLen; i++) {
                hash1 = rollHash(hash1, magic, charText[i - 1], charText[i + t - 1]);
                hash2 = rollHash(hash2, magic, charText[i + t - 1], charText[i + 2 * t - 1]);
                if (hash1 == hash2 && stringsMatch(charText, i, t)) {
                    results.add(hash1);
                }
            }
        }
        return results.size();
    }

    static long initHash(char[] text, int start, int end) {
        long hash = 0;
        for (int j = start; j <= end; j++) {
            hash = (hash * base + text[j]) % p;
        }
        return hash;
    }

    static long rollHash(long oldHash, long magic, int oldChar, int newChar) {
        long rollingHash;
        // magic is sufficient because magic = base^windowLen = base^windowLen-1 * base
        rollingHash = (oldHash * base - oldChar * magic + newChar) % p;
        rollingHash = (rollingHash + p) % p; // in case hash turns negative
        return rollingHash;
    }

    static boolean stringsMatch(char[] text, int start, int windowLen) {
        for (int k = start; k < start + windowLen; k++) {
            if (text[k] != text[k + windowLen]) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        String text = "abcdklqstcdghiklmqghijqrwyzijmnoqrsw";
        System.out.println(distinctEchoSubstrings(text)); // 0
        String text1 = "abcabcabcabcabcabcabcabcabc";
        System.out.println(distinctEchoSubstrings(text1)); // 12
    }
}
