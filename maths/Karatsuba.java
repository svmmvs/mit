package maths;

import java.math.BigInteger;
import java.util.Random;

/*
    only 3 multiplications instead of 4
    T(n) = 3*T(n/2) + O(n) = O(n^log(3)) = O(n^1.58)
 */
public class Karatsuba {

    public static void main(String[] args) {
        long start, end;
        Random random = new Random();
        int N = 20;
        BigInteger a = new BigInteger(N, random);
        BigInteger b = new BigInteger(N, random);
        System.out.println(a + " * " + b);

        start = System.currentTimeMillis();
        BigInteger c = karatsuba(a, b);
        end = System.currentTimeMillis();
        System.out.println("karatsuba: " + c);
        System.out.println("karatsuba time: " + (end - start));

        start = System.currentTimeMillis();
        BigInteger d = a.multiply(b);
        end = System.currentTimeMillis();
        System.out.println("multiplication: " + d);
        System.out.println("multiplication time: " + (end - start));
    }

    /*
        2 n-digit numbers, x and y < radix^n = 2^n
        x = x0 + x1*radix^(n/2)
        y = y0 + y1*radix^(n/2)

        karatsuba terms:
        z0 = x0*y0
        z2 = x1*y1
        z1 = (x0 + x1)*(y0 + y1) - z0 - z2

        karatsuba formula:
        z = x*y = z0 + z1*radix^(n/2) + z2*radix^n
     */
    private static BigInteger karatsuba(BigInteger x, BigInteger y) {
        int n = Math.max(x.bitLength(), y.bitLength());
        // small enough for multiplication
        if (n <= 8) {
            return x.multiply(y);
        }
        // number of bits/2 and round up
        n = (n / 2) + (n % 2);

        BigInteger x1 = x.shiftRight(n);
        BigInteger x0 = x.subtract(x1.shiftLeft(n));
        BigInteger y1 = y.shiftRight(n);
        BigInteger y0 = y.subtract(y1.shiftLeft(n));

        // karatsuba terms
        BigInteger z0 = karatsuba(x0, y0);
        BigInteger z2 = karatsuba(x1, y1);
        BigInteger z1 = karatsuba(x0.add(x1), y0.add(y1)).subtract(z0).subtract(z2);

        // karatsuba formula
        return z0.add(z1.shiftLeft(n)).add(z2.shiftLeft(2 * n));
    }
}
