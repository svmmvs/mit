package maths;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/*
randomly populate an N x M board with exactly k ones and print (no return)
example: width N = 3, height M = 4, k = 2 number of ones
 0, 0, 0,
 0, 0, 1,
 0, 1, 0,
 0, 0, 0,
 */
public class RandomNoDuplicate {

    // 1. consider edge case k too big or negative
    // 2. clarify uniform distribution of random
    public static void main(String[] args) {
        int n = 4;
        int m = 4;
        int k = -1;
        System.out.println("fail: ");
        setup(n, m, k);
        System.out.println("success: ");
        setup(n, m, 14);
    }

    // O(n*m*k)
    private static void setup(int n, int m, int k) {
        if (k > n * m || k < 0) {
            System.out.println("k invalid");
            return;
        }
        int[][] matrix = new int[n][m];
        List<Pair> list = new ArrayList<>(n * m);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                list.add(new Pair(i, j));
            }
        }
        Random rng = new Random();
        for (int i = 0; i < k; i++) {
            int randomIndex = rng.nextInt(list.size());
            Pair random = list.get(randomIndex);
            matrix[random.n][random.m] = 1;
            // remove entry to avoid duplicate (matrix[i][j] already set to 1)
            list.remove(randomIndex);
        }
        printing(n, m, matrix);
    }

    private static void printing(int n, int m, int[][] matrix) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                String s = matrix[i][j] + ", ";
                System.out.print(s);
            }
            System.out.println();
        }
    }

    static class Pair {
        int n;
        int m;

        Pair(int n, int m) {
            this.n = n;
            this.m = m;
        }
    }
}
