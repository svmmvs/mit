package maths;

import java.nio.ByteBuffer;

public class FastInverseSqrt {

    private static final ByteBuffer buffer = ByteBuffer.allocateDirect(4);

    private static float normalInverseSqrt(float x) {
        return 1.0f / (float) Math.sqrt(x);
    }

    /**
     * float32 applied (input only normalized numbers)
     * 1 sign bit (always 0 because only positive numbers)
     * 8 exponent bits 0...255 (but also negative exponents, so 128...-127)
     * 23 mantissa bits (for 1...2)
     * <p>
     * log2[ (1 + mantissa/2^23) * 2^(exponent - 127) ]
     * = log2[ ( 1 + mantissa/2^23) ] + exponent - 127 , for small x log2[1+x] = x + C
     * = mantissa/2^23 + C + exponent - 127
     * = mantissa/2^23 + exponent + C - 127
     * = mantissa/2^23 + (2^23 * exponent)/2^23 + C - 127
     * = 1/2^23 * (mantissa + 2^23 * exponent) + C - 127
     * = bit representation * C1 + C2
     **/
    private static float fastInverseSqrt(float x) {
        float x_half = 0.5f * x;
        // no bit manipulation possible on floats, but on int (32 bits)
        // take over bits to int (not number conversion but address conversion)
        // i approximately log[x]
        int i = Float.floatToIntBits(x);
        // bit shift to right is 1/2, so -1/2
        // log[1/sqrt(x)] = log[x^(-1/2)] = -1/2 * log[x] = -1/2 * i
        // 0x5f3759df results out of C1 C2 terms
        i = 0x5f3759df - (i >> 1);
        // receive approximate solution back from bits
        float w = Float.intBitsToFloat(i);
        // f(w) = 0 = 1/w^2 - x => x = 1/w^2 => w = 1/sqrt(x)
        // newton iteration w = w - [f(w)/f'(w)]
        return w * (1.5f - (x_half * w * w));
    }

    private static float fastInverseSqrtBuffer(float x) {
        float x_half = 0.5f * x;
        int i = buffer.putFloat(0, x).getInt(0);
        i = 0x5f375a86 - (i >> 1);
        float w = buffer.putInt(0, i).getFloat(0);
        return w * (1.5f - (x_half * w * w));
    }

    public static void main(String[] args) {
        float[] data = new float[10000];
        for (int i = 0; i < data.length; i++) {
            data[i] = (float) Math.random() * 1000 + 0.000001f;
        }
        long t0 = 0, t1 = 0, t2 = 0, t3 = 0;
        float r = 0, g = 0, b = 0;

        for (int i = 0; i < 100; i++) {
            r = g = b = 0;
            t0 = System.nanoTime();
            for (float x : data) {
                r += normalInverseSqrt(x);
            }
            t1 = System.nanoTime();
            for (float x : data) {
                g += fastInverseSqrt(x);
            }
            t2 = System.nanoTime();
            for (float x : data) {
                b += fastInverseSqrtBuffer(x);
            }
            t3 = System.nanoTime();
        }

        System.out.println("Normal: " + (t1 - t0) + " Result = " + r);
        System.out.println("Fast: " + (t2 - t1) + " Result = " + g);
        System.out.println("Fast Buffer: " + (t3 - t2) + " Result = " + b);
    }
}
