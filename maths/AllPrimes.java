package maths;

import java.util.ArrayList;
import java.util.List;

// prime numbers: given input integer, return all prime integers smaller than input
public class AllPrimes {

    public static void main(String[] args) {
        System.out.println(primes1(32));
        System.out.println(primes2(32));
    }

    // O(n^2)
    public static List<Integer> primes1(int given) {
        List<Integer> res = new ArrayList<>();
        // 0,1 not prime, check numbers starting at 2
        for (int k = 2; k < given; k++) {
            boolean prime = true;
            // check from 2 to sqrt(k)
            for (int i = 2; i * i <= k; i++) {
                if (k % i == 0) {
                    prime = false;
                    break;
                }
            }
            if (prime) res.add(k);
        }
        return res;
    }

    // O(n*log(log(n)))
    public static List<Integer> primes2(int given) {
        boolean[] prime = new boolean[given + 1];
        for (int i = 0; i <= given; i++) {
            prime[i] = true;
        }
        List<Integer> res = new ArrayList<>();
        // 0,1 not prime, check numbers starting at 2
        for (int k = 2; k < given; k++) {
            // check from 2 to sqrt(k)
            for (int i = 2; i * i <= k; i++) {
                if (prime[i]) {
                    // update all multiples of i; can start from i*i and not 2*i,
                    // because any numbers j*i (for j < i) already marked as non-prime when at j.
                    for (int m = i * i; m <= given; m += i) {
                        prime[m] = false;
                    }
                }
            }
            if (prime[k]) res.add(k);
        }
        return res;
    }
}
