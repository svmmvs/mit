package maths;

/**
 * insert digits of a number and insert digit 5 to get the maximum possible absolute value
 * <p>
 * For example, if the input is 778 the expected result is 7785,
 * input 123, result is 5123
 * Otherwise, if the input is negative -483, the output is expected to be -4583
 */
public class Digit5 {

    // 1 if number digit is smaller than digit, insert
    // 2 if number digit is >= digit, move to next number digit, until 1
    // if negative number, the opposite logic
    public static String insertDigit5(int number) {
        int digit = 5;
        String num = String.valueOf(number);
        boolean negative = num.charAt(0) == '-';

        for (int i = 0; i < num.length(); i++) {
            if (!negative && num.charAt(i) - '0' < digit || negative && num.charAt(i) - '0' > digit) {
                return num.substring(0, i) + digit + num.substring(i);
            }
        }
        return num + digit;
    }

    public static void main(String[] args) {
        int N = 578;
        System.out.println(insertDigit5(N));
    }
}
