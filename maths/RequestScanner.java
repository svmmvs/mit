package maths;

import java.util.*;
import java.util.stream.Collectors;

/*
Example input:
; <timestamp in ms> <requested URL> <HTTP response code> <duration in ms>

1 /index.html 200 150
2 /article/1234.html 500 1
5 /index.html 200 50
1000 /article/4321.html 200 10

Example output:
1 -> 1 ; got an index.html which will be running between 1..151 ms
2 -> 2 ; still got the index.html from above, plus article/1234.html running between 2..3 ms
5 -> 2 ; still got the index.html from above; article/1234.html is already done at 3ms; got yet another index.html 5..55 ms
1000 -> 1 ; all older requests are done; only got article/4321.html between 1000..1010 ms
*/
public class RequestScanner {

    public static void main(String[] args) {
        String input = String.join("\n",
                "1 /index.html 200 150",
                "1 /index.html 200 150",
                "1 /index.html 200 150",
                "2 /article/1234.html 500 1",
                "2 /article/1234.html 500 1",
                "2 /article/1234.html 500 1",
                "5 /index.html 200 50",
                "1000 /article/4321.html 200 10",
                "1000 /article/4321.html 200 10",
                "1000 /article/4321.html 200 10");
        OutputWriter writer = new OutputWriter();
        System.out.println("Fast version");
        new OutputPrinter().solve(input, writer);
        System.out.println("Slow version");
        new OutputPrinterSlow().solve(input, writer);
    }

    static class OutputWriter {
        public void printResult(long timestamp, int count) {
            System.out.println(timestamp + " -> " + count);
        }
    }

    static class LogEntry {
        long timeStamp;
        String url;
        String httpCode;
        long duration;

        LogEntry(String logLine) {
            String[] split = logLine.split("\\s+");
            timeStamp = Long.parseLong(split[0]);
            url = split[1];
            httpCode = split[2];
            duration = Long.parseLong(split[3]);
        }

        public long getTimeStamp() {
            return timeStamp;
        }

        public long getDuration() {
            return duration;
        }
    }

    /*
     * time complexity with heap is O(n*log(n))
     *    while(timestamp >= heap.peek()) {
     *        heap.remove();
     *    }
     */
    static class OutputPrinter {
        public void solve(String input, OutputWriter output) {
            // need sorted list in ascending order of ending times to compare time stamps
            PriorityQueue<Long> endings = new PriorityQueue<>();
            long timeChange = 0;
            try (Scanner scanner = new Scanner(input)) {
                while (scanner.hasNextLine()) {
                    String scan = scanner.nextLine();
                    LogEntry currentLine = new LogEntry(scan);
                    long currentStamp = currentLine.getTimeStamp();
                    long currentDuration = currentLine.getDuration();
                    long currentEnding = currentStamp + currentDuration;

                    // first time stamp
                    if (timeChange == 0) {
                        timeChange = currentStamp;
                    }

                    // only log when time changes
                    if (timeChange != currentStamp) {
                        output.printResult(timeChange, endings.size());
                        // update time
                        timeChange = currentStamp;
                    }

                    endings.add(currentEnding);
                    Iterator<Long> iterator = endings.iterator();
                    while (iterator.hasNext() && currentStamp >= iterator.next()) {
                        iterator.remove();
                    }

                    // last log because request counter cannot increase anymore
                    if (!scanner.hasNextLine()) {
                        output.printResult(timeChange, endings.size());
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /*
     * time complexity with list is O(n^2)
     */
    static class OutputPrinterSlow {
        public void solve(String input, OutputWriter output) {
            List<Long> endings = new ArrayList<>();
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String scan = scanner.nextLine();
                LogEntry currentLine = new LogEntry(scan);
                long currentStamp = currentLine.getTimeStamp();
                long currentDuration = currentLine.getDuration();
                long currentEnding = currentStamp + currentDuration;
                endings.add(currentEnding);
                // will remove everything smaller than `currentStamp` from the array
                endings = removeOldTimestamps(endings, currentStamp);
                output.printResult(currentStamp, endings.size());
            }
            scanner.close();
        }

        private List<Long> removeOldTimestamps(List<Long> array, long timestamp) {
            return array.stream()
                    .filter(n -> n > timestamp)
                    .collect(Collectors.toList());
        }
    }
}
