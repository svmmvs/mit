package maths;

public class StringAdjustment {
    /**
     * You are given 2 strings of the same length, consisting of 0-1. We call them string A and string B. Now we want to make them become the same string. We only have one operation. That is to select one character in A and swap with another character in B (You can choose any position in the string). Then what is the minimum number of operations to make the strings become the same string.
     * <p>
     * Sample 1:
     * Input:
     * 0101
     * 0011
     * Output: 2
     * <p>
     * hint:
     * step 1
     * 0001
     * 0111
     * step 2
     * 0011
     * 0011
     * <p>
     * Sample 2:
     * Input:
     * 10110011
     * 01010100
     * Output:
     * 3
     * <p>
     * hint
     * step 1
     * 10010011
     * 11010100
     * step 2
     * 10010111
     * 10010100
     * step 3
     * 10010101
     * 10010101
     */
    public static void main(String[] args) {
        String str1 = "0101";
        String str2 = "0011";
        // Candidate 1
        System.out.println("Candidate 1 Output: " + swapStrings(str1.toCharArray(), str2.toCharArray()));
        // Candidate 2
        System.out.println("Candidate 2 Output: " + swapStrings2(str1, str2));

        str1 = "10110011";
        str2 = "01010100";
        // Candidate 1
        System.out.println("Candidate 1 Output: " + swapStrings(str1.toCharArray(), str2.toCharArray()));
        // Candidate 2
        System.out.println("Candidate 2 Output: " + swapStrings2(str1, str2));
    }

    private static int swapStrings(char[] arr1, char[] arr2) {
        if (arr1.length != arr2.length) {
            throw new IllegalArgumentException("Strings must be of the same length");
        }

        int count10 = 0; // Mismatches where arr1 has '1' and arr2 has '0'
        int count01 = 0; // Mismatches where arr1 has '0' and arr2 has '1'
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] == '1' && arr2[i] == '0') {
                count10++;
            } else if (arr1[i] == '0' && arr2[i] == '1') {
                count01++;
            }
        }
        int pairSwaps = Math.min(count10, count01);
        int remainingMismatches = Math.abs(count10 - count01);
        int additionalSwaps = (remainingMismatches + 1) / 2;

        return pairSwaps + additionalSwaps;
    }

    private static int swapStrings2(String str1, String str2) {
        if (str1.length() != str2.length()) {
            throw new IllegalArgumentException("Strings must be of the same length");
        }

        int count10 = 0; // Count where str1 has '1' and str2 has '0'
        int count01 = 0; // Count where str1 has '0' and str2 has '1'

        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) == '1' && str2.charAt(i) == '0') {
                count10++;
            } else if (str1.charAt(i) == '0' && str2.charAt(i) == '1') {
                count01++;
            }
        }

        int swaps = Math.min(count10, count01);
        int remainingMismatches = Math.abs(count10 - count01);
        swaps += remainingMismatches;

        return swaps;
    }
}
