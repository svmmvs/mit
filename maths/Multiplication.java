package maths;

/*
two non-negative integers num1 and num2 represented as strings,
return product of num1 and num2 represented as string.
(no direct conversion to integer)
example:
num1 = "123", num2 = "456", return "56088"

123*456
    738
   615
  492
= 56088

1 <= num1.length, num2.length <= 200
num1 and num2 digits only, no leading zeros, except number zero itself
*/
public class Multiplication {

    public static void main(String[] args) {
        String num1 = "123";
        String num2 = "456";
        System.out.println("multiply like school: " + multiplyLikeSchool(num2, num1));
        System.out.println("multiply: " + multiply(num2, num1));
    }

    public static String multiplyLikeSchool(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        if (num1.length() < num2.length()) {
            String temp = num2;
            num2 = num1;
            num1 = temp;
        }
        int len1 = num1.length();
        int len2 = num2.length();
        int[][] sums = new int[len1][len2];

        // calculate all lines which will be used for column wise addition
        for (int i = len1 - 1; i >= 0; i--) {
            for (int j = len2 - 1; j >= 0; j--) {
                int product = Character.getNumericValue(num1.charAt(i)) * Character.getNumericValue(num2.charAt(j));
                if (j == len2 - 1) {
                    sums[i][j] = product;
                } else {
                    sums[i][j] = product + sums[i][j + 1] / 10;
                    sums[i][j + 1] = sums[i][j + 1] % 10;
                }
            }
        }

        // addition through all rows, one column after another
        int[] res = new int[len1 + len2 - 1];
        if (sums.length == 1) {
            res = sums[0];
        } else {
            int k = res.length - 1;
            int row = sums.length - 1;
            int col = sums[0].length - 1;
            // guarantee due to longer number swap, rows >= columns in sums
            // left number capped by len2, right number capped by len1 (num2 * num1)
            int outerIndex = k;
            int innerIndex = k;
            for (int r = row; r >= 0; r--) {
                for (int c = col; c >= 0; c--) {
                    res[innerIndex] = res[innerIndex] + sums[r][c];
                    innerIndex--;
                }
                outerIndex--;
                // reset the innerIndex for columns to the current rightmost res sum (decreases by one every row)
                innerIndex = outerIndex;
            }
        }

        // carries to get to the final result
        for (int s = res.length - 1; s > 0; s--) {
            res[s - 1] = res[s - 1] + res[s] / 10;
            res[s] = res[s] % 10;
        }

        StringBuilder builder = new StringBuilder();
        for (int digit : res) {
            builder.append(digit);
        }
        return builder.toString();
    }

    /*
    123*456     index
         18     4
        12      3
        6       2
        15      3
       10       2
       5        1
       12       2
       8        1
      4         0
    = 56088
    */
    public static String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        int len1 = num1.length();
        int len2 = num2.length();
        int[] result = new int[len1 + len2 - 1];

        for (int i = len1 - 1; i >= 0; i--) {
            for (int j = len2 - 1; j >= 0; j--) {
                int product = Character.getNumericValue(num1.charAt(i)) * Character.getNumericValue(num2.charAt(j));
                result[i + j] += product;
            }
        }

        for (int s = result.length - 1; s > 0; s--) {
            result[s - 1] = result[s - 1] + result[s] / 10;
            result[s] = result[s] % 10;
        }

        StringBuilder builder = new StringBuilder();
        for (int d : result) {
            builder.append(d);
        }
        return builder.toString();
    }
}
